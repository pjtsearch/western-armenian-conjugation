import isVowel from "./utilities/isVowel"

interface conjugationObject {
  present:string[]
  imperfect:string[]
  past:string[]
  imperative:Array<string|undefined>
  presentPerfect:string[]
  pluperfect:string[]
  future:string[]
  conditional:string[]
}

interface irregularitiesObject {
  infinitive:string;
  root:string;
  negativeParticiple:string;
  group:number|null;
  positiveConjugations:conjugationObject;
  negativeConjugations:conjugationObject|null;
  particle:string;
  pastParticiple:string;
}

export default class Word {
  infinitive:string;
  root:string;
  group:number|null = null;
  positiveConjugations:conjugationObject;
  negativeConjugations:conjugationObject|null = null;
  particle:string;
  pastParticiple:string;
  futureParticiple:string;
  negativeParticiple:string;
  imperfectNegativeParticiple:string;
  irregularities:conjugationObject|null = null;

  constructor(word:string,irregularities?:irregularitiesObject){
    this.infinitive = word
    this.irregularities = irregularities ? irregularities.positiveConjugations : null
    this.root = irregularities?.root ? irregularities.root : this._getRoot()
    this.futureParticiple = this._getRoot()
    this.particle = irregularities?.particle ? irregularities.particle : this._getParticle()
    this.pastParticiple = irregularities?.pastParticiple ? irregularities.pastParticiple : this._getPastParticiple();
    this.negativeParticiple = irregularities?.negativeParticiple ? irregularities.negativeParticiple : this._getNegativeParticiple();
    //
    // if negativeParticiple, then uses it as imperfectNegativeParticiple
    //
    this.imperfectNegativeParticiple = irregularities?.negativeParticiple ? irregularities.negativeParticiple : this._getImperfectNegativeParticiple()
    this.positiveConjugations = this._getPositiveConjugations()
    if (irregularities?.positiveConjugations){
      this.positiveConjugations = this._mergeIrregularConjugations(this.positiveConjugations,irregularities.positiveConjugations)
    }
    this.negativeConjugations = this._getNegativeConjugations()
  }
  _getPositiveConjugations():conjugationObject{
    return {
      present:this._getPositivePresentConjugations(),
      imperfect:this._getPositiveImperfectConjugations(),
      past:this._getPositivePastConjugations(),
      imperative:this._getPositiveImperativeConjugations(),
      presentPerfect:this._getPositivePresentPerfectConjugations(),
      pluperfect:this._getPositivePluperfectConjugations(),
      future:this._getPositiveFutureConjugations(),
      conditional:this._getPositiveConditionalConjugations()
    }
  }
  _getNegativeConjugations():conjugationObject{
    return {
      present:this._getNegativePresentConjugations(),
      imperfect:this._getNegativeImperfectConjugations(),
      past:this._getNegativePastConjugations(),
      imperative:this._getNegativeImperativeConjugations(),
      presentPerfect:this._getNegativePresentPerfectConjugations(),
      pluperfect:this._getNegativePluperfectConjugations(),
      future:this._getNegativeFutureConjugations(),
      conditional:this._getNegativeConditionalConjugations()
    }
  }
  _getParticle():string{
    if (isVowel(this.infinitive.charAt(0))){
      return "Կ\'"
    }else if (this.infinitive.length > 3){
      return "կը "
    // else if (this.group === 0){
    //   return ""
    // }
    }else{
      return "կու "
    }
  }
  _mergeIrregularConjugations(orig:conjugationObject,irr:conjugationObject):conjugationObject{
    var merged:any = {
      present:[],
      imperfect:[],
      past:[],
      imperative:[],
      presentPerfect:[],
      pluperfect:[],
      future:[],
      conditional:[]
    }
    Object.keys(orig).forEach((categoryKey)=>{
      //FIX
      //@ts-ignore
      //
      let category = orig[categoryKey]
      category.forEach((conjugation:string,conjugationIndex:number)=>{
        //FIX
        //@ts-ignore
        //
        merged[categoryKey][conjugationIndex] = irr[categoryKey][conjugationIndex] ? irr[categoryKey][conjugationIndex] : conjugation
      })
    })
    return merged
  }
  _getRoot():string{return ""}
  _getPastParticiple():string{return ""}
  _getNegativeParticiple():string{return ""}
  _getImperfectNegativeParticiple():string{return ""}
  _getPositivePresentConjugations():string[]{return []}
  _getPositiveImperfectConjugations():string[]{return []}
  _getPositivePastConjugations():string[]{return []}
  _getPositiveImperativeConjugations():Array<string|undefined>{return []}
  _getPositivePresentPerfectConjugations():string[]{return []}
  _getPositivePluperfectConjugations():string[]{return []}
  _getPositiveFutureConjugations():string[]{return []}
  _getPositiveConditionalConjugations():string[]{return []}

  _getNegativePresentConjugations():string[]{return []}
  _getNegativeImperfectConjugations():string[]{return []}
  _getNegativePastConjugations():string[]{return []}
  _getNegativeImperativeConjugations():Array<string|undefined>{return []}
  _getNegativePresentPerfectConjugations():string[]{return []}
  _getNegativePluperfectConjugations():string[]{return []}
  _getNegativeFutureConjugations():string[]{return []}
  _getNegativeConditionalConjugations():string[]{return []}
}

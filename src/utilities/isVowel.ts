const isVowel = (character:string) => ["ա","ե","է","ը","ի","ո","օ"].includes(character)
export default isVowel

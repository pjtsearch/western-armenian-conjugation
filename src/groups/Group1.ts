import Word from "../Word"
import isVowel from "../utilities/isVowel"

export default class Group1 extends Word {
  constructor(word:string,irregularities?:any){
    super(word,irregularities)
    this.group = 1
  }
  _getRoot(){
    let [_,root] = this.infinitive.match(/(.+)ել/)
    return root
  }
  _getPastParticiple(){
    if (this.root.slice(-2) === "ցն"){
      return `${this.root.slice(0,-1)}ուցած`
    } else if (this.root.slice(-1) === "ն"){
      return `${this.root}եցած`
    } else{
      return `${this.root}ած`
    }
  }
  _getNegativeParticiple(){
    return `${this.root}եր`
  }
  _getImperfectNegativeParticiple(){
    return `${this.root}եր`
  }
  _getPositivePresentConjugations(){
    let first,second,third,firstPlural,secondPlural,thirdPlural;
    let base = `${this.particle}${this.root}`
    first = `${base}եմ`
    second = `${base}ես`
    third = `${base}է`
    firstPlural = `${base}ենք`
    secondPlural = `${base}էք`
    thirdPlural = `${base}են`
    return [first,second,third,firstPlural,secondPlural,thirdPlural]
  }
  _getPositiveImperfectConjugations(){
    let first,second,third,firstPlural,secondPlural,thirdPlural;
    let base = `${this.particle}${this.root}`
    first = `${base}էի`
    second = `${base}էիր`
    third = `${base}էր`
    firstPlural = `${base}էինք`
    secondPlural = `${base}էիք`
    thirdPlural = `${base}էին`
    return [first,second,third,firstPlural,secondPlural,thirdPlural]
  }
  _getPositivePastConjugations(){
    let first,second,third,firstPlural,secondPlural,thirdPlural;
    let base = `${this.root}`
    if (base.slice(-2) !== "ցն"){
      first = `${base}եցի`
      second = `${base}եցիր`
      third = `${base}եց`
      firstPlural = `${base}եցինք`
      secondPlural = `${base}եցիք`
      thirdPlural = `${base}եցին`
    }else{
      first = `${base.slice(0, -1)}ուցի`
      second = `${base.slice(0, -1)}ուցիր`
      third = `${base.slice(0, -1)}ուց`
      firstPlural = `${base.slice(0, -1)}ուցինք`
      secondPlural = `${base.slice(0, -1)}ուցիք`
      thirdPlural = `${base.slice(0, -1)}ուցին`
    }
    return [first,second,third,firstPlural,secondPlural,thirdPlural]
  }
  _getPositiveImperativeConjugations(){
    let first,second,third,firstPlural,secondPlural,thirdPlural;
    let base = `${this.root}`
		if (base.slice(-2) !== "ցն"){
			second = `${base}է`
			firstPlural = `${base}ենք`
			secondPlural = `${base}եցէք`
		}else{
			second = `${base.slice(0, -1)}ուր`
			firstPlural = `${base}ենք`
			secondPlural = `${base.slice(0, -1)}ուցէք`
		}
    return [first,second,third,firstPlural,secondPlural,thirdPlural]
  }
  _getPositivePresentPerfectConjugations(){
    let first,second,third,firstPlural,secondPlural,thirdPlural;
    let base = this.pastParticiple;
    first = `${base} եմ`
    second = `${base} ես`
    third = `${base} է`
    firstPlural = `${base} ենք`
    secondPlural = `${base} եք`
    thirdPlural = `${base} են`
    return [first,second,third,firstPlural,secondPlural,thirdPlural]
  }
  _getPositivePluperfectConjugations(){
    let first,second,third,firstPlural,secondPlural,thirdPlural;
    let base = this.pastParticiple;
    first = `${base} էի`
    second = `${base} էիր`
    third = `${base} էր`
    firstPlural = `${base} էինք`
    secondPlural = `${base} էիք`
    thirdPlural = `${base} էին`
    return [first,second,third,firstPlural,secondPlural,thirdPlural]
  }
  _getPositiveFutureConjugations(){
    let first,second,third,firstPlural,secondPlural,thirdPlural;
    let base = `պիտի ${this.futureParticiple}`
    first = `${base}եմ`
    second = `${base}ես`
    third = `${base}է`
    firstPlural = `${base}ենք`
    secondPlural = `${base}էք`
    thirdPlural = `${base}են`
    return [first,second,third,firstPlural,secondPlural,thirdPlural]
  }
  _getPositiveConditionalConjugations(){
    let first,second,third,firstPlural,secondPlural,thirdPlural;
    let base = `պիտի ${this.futureParticiple}`
    first = `${base}էի`
    second = `${base}էիր`
    third = `${base}էր`
    firstPlural = `${base}էինք`
    secondPlural = `${base}էիք`
    thirdPlural = `${base}էին`
    return [first,second,third,firstPlural,secondPlural,thirdPlural]
  }



  _getNegativePresentConjugations(){
    let first,second,third,firstPlural,secondPlural,thirdPlural;
    let base = `${this.negativeParticiple}`
    first = `չեմ ${base}`
    second = `չես ${base}`
    third = isVowel(base.charAt(0)) ? `չ'${base}` : `չի ${base}`
    firstPlural = `չենք ${base}`
    secondPlural = `չեք ${base}`
    thirdPlural = `չեն ${base}`
    return [first,second,third,firstPlural,secondPlural,thirdPlural]
  }
  _getNegativeImperfectConjugations(){
    let first,second,third,firstPlural,secondPlural,thirdPlural;
    let base = `${this.imperfectNegativeParticiple}`
    first = `չէի ${base}`
    second = `չէիր ${base}`
    third = `չէր ${base}`
    firstPlural = `չէինք ${base}`
    secondPlural = `չէիք ${base}`
    thirdPlural = `չէին ${base}`
    return [first,second,third,firstPlural,secondPlural,thirdPlural]
  }
  _getNegativePastConjugations(){
    let first,second,third,firstPlural,secondPlural,thirdPlural;
    let base = this.positiveConjugations.past;
    first = `չ${base[0]}`
    second = `չ${base[1]}`
    third = `չ${base[2]}`
    firstPlural = `չ${base[3]}`
    secondPlural = `չ${base[4]}`
    thirdPlural = `չ${base[5]}`
    return [first,second,third,firstPlural,secondPlural,thirdPlural]
  }
  _getNegativeImperativeConjugations(){
    let first,second,third,firstPlural,secondPlural,thirdPlural;
    let base = `${this.futureParticiple}`
    second = `մի ${this.negativeParticiple}`
    firstPlural = `մի ${base}ենք`
    secondPlural = `մի ${base}էք`
    return [first,second,third,firstPlural,secondPlural,thirdPlural]
  }
  _getNegativePresentPerfectConjugations(){
    let first,second,third,firstPlural,secondPlural,thirdPlural;
    let base = `${this.pastParticiple}`
    first = `${base} չեմ`
    second = `${base} չես`
    third = `${base} չէ`
    firstPlural = `${base} չենք`
    secondPlural = `${base} չեք`
    thirdPlural = `${base} չեն`
    return [first,second,third,firstPlural,secondPlural,thirdPlural]
  }
  _getNegativePluperfectConjugations(){
    let first,second,third,firstPlural,secondPlural,thirdPlural;
    let base = `${this.pastParticiple}`
    first = `${base} չէի`
    second = `${base} չէիր`
    third = `${base} չէր`
    firstPlural = `${base} չէինք`
    secondPlural = `${base} չէիք`
    thirdPlural = `${base} չէին`
    return [first,second,third,firstPlural,secondPlural,thirdPlural]
  }
  _getNegativeFutureConjugations(){
    let first,second,third,firstPlural,secondPlural,thirdPlural;
    let base = `պիտի չ${this.futureParticiple}`
    first = `${base}եմ`
    second = `${base}ես`
    third = `${base}է`
    firstPlural = `${base}ենք`
    secondPlural = `${base}էք`
    thirdPlural = `${base}են`
    return [first,second,third,firstPlural,secondPlural,thirdPlural]
  }
  _getNegativeConditionalConjugations(){
    let first,second,third,firstPlural,secondPlural,thirdPlural;
    let base = `պիտի չ${this.futureParticiple}`
    first = `${base}էի`
    second = `${base}էիր`
    third = `${base}էր`
    firstPlural = `${base}էինք`
    secondPlural = `${base}էիք`
    thirdPlural = `${base}էին`
    return [first,second,third,firstPlural,secondPlural,thirdPlural]
  }
}

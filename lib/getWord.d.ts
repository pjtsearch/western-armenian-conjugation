import { Group1, Group2, Group3 } from "./groups";
declare const getWord: (word: string) => Group1 | Group2 | Group3 | undefined;
export default getWord;

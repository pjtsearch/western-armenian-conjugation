interface conjugationObject {
    present: string[];
    imperfect: string[];
    past: string[];
    imperative: Array<string | undefined>;
    presentPerfect: string[];
    pluperfect: string[];
    future: string[];
    conditional: string[];
}
interface irregularitiesObject {
    infinitive: string;
    root: string;
    negativeParticiple: string;
    group: number | null;
    positiveConjugations: conjugationObject;
    negativeConjugations: conjugationObject | null;
    particle: string;
    pastParticiple: string;
}
export default class Word {
    infinitive: string;
    root: string;
    group: number | null;
    positiveConjugations: conjugationObject;
    negativeConjugations: conjugationObject | null;
    particle: string;
    pastParticiple: string;
    futureParticiple: string;
    negativeParticiple: string;
    imperfectNegativeParticiple: string;
    irregularities: conjugationObject | null;
    constructor(word: string, irregularities?: irregularitiesObject);
    _getPositiveConjugations(): conjugationObject;
    _getNegativeConjugations(): conjugationObject;
    _getParticle(): string;
    _mergeIrregularConjugations(orig: conjugationObject, irr: conjugationObject): conjugationObject;
    _getRoot(): string;
    _getPastParticiple(): string;
    _getNegativeParticiple(): string;
    _getImperfectNegativeParticiple(): string;
    _getPositivePresentConjugations(): string[];
    _getPositiveImperfectConjugations(): string[];
    _getPositivePastConjugations(): string[];
    _getPositiveImperativeConjugations(): Array<string | undefined>;
    _getPositivePresentPerfectConjugations(): string[];
    _getPositivePluperfectConjugations(): string[];
    _getPositiveFutureConjugations(): string[];
    _getPositiveConditionalConjugations(): string[];
    _getNegativePresentConjugations(): string[];
    _getNegativeImperfectConjugations(): string[];
    _getNegativePastConjugations(): string[];
    _getNegativeImperativeConjugations(): Array<string | undefined>;
    _getNegativePresentPerfectConjugations(): string[];
    _getNegativePluperfectConjugations(): string[];
    _getNegativeFutureConjugations(): string[];
    _getNegativeConditionalConjugations(): string[];
}
export {};

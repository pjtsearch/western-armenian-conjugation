"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var isVowel_1 = require("./utilities/isVowel");
var Word = /** @class */ (function () {
    function Word(word, irregularities) {
        var _a, _b, _c, _d, _e, _f;
        this.group = null;
        this.negativeConjugations = null;
        this.irregularities = null;
        this.infinitive = word;
        this.irregularities = irregularities ? irregularities.positiveConjugations : null;
        this.root = ((_a = irregularities) === null || _a === void 0 ? void 0 : _a.root) ? irregularities.root : this._getRoot();
        this.futureParticiple = this._getRoot();
        this.particle = ((_b = irregularities) === null || _b === void 0 ? void 0 : _b.particle) ? irregularities.particle : this._getParticle();
        this.pastParticiple = ((_c = irregularities) === null || _c === void 0 ? void 0 : _c.pastParticiple) ? irregularities.pastParticiple : this._getPastParticiple();
        this.negativeParticiple = ((_d = irregularities) === null || _d === void 0 ? void 0 : _d.negativeParticiple) ? irregularities.negativeParticiple : this._getNegativeParticiple();
        //
        // if negativeParticiple, then uses it as imperfectNegativeParticiple
        //
        this.imperfectNegativeParticiple = ((_e = irregularities) === null || _e === void 0 ? void 0 : _e.negativeParticiple) ? irregularities.negativeParticiple : this._getImperfectNegativeParticiple();
        this.positiveConjugations = this._getPositiveConjugations();
        if ((_f = irregularities) === null || _f === void 0 ? void 0 : _f.positiveConjugations) {
            this.positiveConjugations = this._mergeIrregularConjugations(this.positiveConjugations, irregularities.positiveConjugations);
        }
        this.negativeConjugations = this._getNegativeConjugations();
    }
    Word.prototype._getPositiveConjugations = function () {
        return {
            present: this._getPositivePresentConjugations(),
            imperfect: this._getPositiveImperfectConjugations(),
            past: this._getPositivePastConjugations(),
            imperative: this._getPositiveImperativeConjugations(),
            presentPerfect: this._getPositivePresentPerfectConjugations(),
            pluperfect: this._getPositivePluperfectConjugations(),
            future: this._getPositiveFutureConjugations(),
            conditional: this._getPositiveConditionalConjugations()
        };
    };
    Word.prototype._getNegativeConjugations = function () {
        return {
            present: this._getNegativePresentConjugations(),
            imperfect: this._getNegativeImperfectConjugations(),
            past: this._getNegativePastConjugations(),
            imperative: this._getNegativeImperativeConjugations(),
            presentPerfect: this._getNegativePresentPerfectConjugations(),
            pluperfect: this._getNegativePluperfectConjugations(),
            future: this._getNegativeFutureConjugations(),
            conditional: this._getNegativeConditionalConjugations()
        };
    };
    Word.prototype._getParticle = function () {
        if (isVowel_1.default(this.infinitive.charAt(0))) {
            return "Կ\'";
        }
        else if (this.infinitive.length > 3) {
            return "կը ";
            // else if (this.group === 0){
            //   return ""
            // }
        }
        else {
            return "կու ";
        }
    };
    Word.prototype._mergeIrregularConjugations = function (orig, irr) {
        var merged = {
            present: [],
            imperfect: [],
            past: [],
            imperative: [],
            presentPerfect: [],
            pluperfect: [],
            future: [],
            conditional: []
        };
        Object.keys(orig).forEach(function (categoryKey) {
            //FIX
            //@ts-ignore
            //
            var category = orig[categoryKey];
            category.forEach(function (conjugation, conjugationIndex) {
                //FIX
                //@ts-ignore
                //
                merged[categoryKey][conjugationIndex] = irr[categoryKey][conjugationIndex] ? irr[categoryKey][conjugationIndex] : conjugation;
            });
        });
        return merged;
    };
    Word.prototype._getRoot = function () { return ""; };
    Word.prototype._getPastParticiple = function () { return ""; };
    Word.prototype._getNegativeParticiple = function () { return ""; };
    Word.prototype._getImperfectNegativeParticiple = function () { return ""; };
    Word.prototype._getPositivePresentConjugations = function () { return []; };
    Word.prototype._getPositiveImperfectConjugations = function () { return []; };
    Word.prototype._getPositivePastConjugations = function () { return []; };
    Word.prototype._getPositiveImperativeConjugations = function () { return []; };
    Word.prototype._getPositivePresentPerfectConjugations = function () { return []; };
    Word.prototype._getPositivePluperfectConjugations = function () { return []; };
    Word.prototype._getPositiveFutureConjugations = function () { return []; };
    Word.prototype._getPositiveConditionalConjugations = function () { return []; };
    Word.prototype._getNegativePresentConjugations = function () { return []; };
    Word.prototype._getNegativeImperfectConjugations = function () { return []; };
    Word.prototype._getNegativePastConjugations = function () { return []; };
    Word.prototype._getNegativeImperativeConjugations = function () { return []; };
    Word.prototype._getNegativePresentPerfectConjugations = function () { return []; };
    Word.prototype._getNegativePluperfectConjugations = function () { return []; };
    Word.prototype._getNegativeFutureConjugations = function () { return []; };
    Word.prototype._getNegativeConditionalConjugations = function () { return []; };
    return Word;
}());
exports.default = Word;

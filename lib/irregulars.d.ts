declare const _default: {
    "infinitive": string;
    "root": string;
    "group": null;
    "particle": string;
    "pastParticiple": string;
    "negativeParticiple": string;
    "positiveConjugations": {
        "present": string[];
        "imperfect": string[];
        "past": string[];
        "imperative": string[];
        "presentPerfect": string[];
        "pluperfect": string[];
        "future": string[];
        "conditional": string[];
    };
}[];
export default _default;

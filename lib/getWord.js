"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var groups_1 = require("./groups");
var irregulars_1 = require("./irregulars");
var getWord = function (word) {
    //{root:String,group:Number}
    //
    // TEST
    //
    //
    // if (word === "ըսել"){
    // 	return new Group1(word,{ "infinitive": "ըսել", "root": "", "group": null, "particle": "", "pastParticiple": "ըսած", "negativeParticiple":"", "positiveConjugations": { "present": { "0": "", "1": "", "2": "", "3": "", "4": "", "5": "" }, "imperfect": { "0": "", "1": "", "2": "", "3": "", "4": "", "5": "" }, "past": { "0": "ըսի", "1": "ըսիր", "2": "ըսաւ", "3": "ըսինք", "4": "ըսիք", "5": "ըսին" }, "imperative": { "0": "", "1": "ըսէ", "2": "", "3": "", "4": "ըսէք", "5": "" }, "presentPerfect": { "0": "", "1": "", "2": "", "3": "", "4": "", "5": "" }, "pluperfect": { "0": "", "1": "", "2": "", "3": "", "4": "", "5": "" }, "future": { "0": "", "1": "", "2": "", "3": "", "4": "", "5": "" }, "conditional": { "0": "", "1": "", "2": "", "3": "", "4": "", "5": "" } }})
    // }else if (word === "կարենալ"){
    // 	return new Group3(word,{ "infinitive": "կարենալ", "root": "կրն", "group": null, "particle": "-", "pastParticiple": "կրցած", "negativeParticiple": "կրնար", "positiveConjugations": { "present": { "0": "կրնամ", "1": "կրնաս", "2": "կրնայ", "3": "կրնանք", "4": "կրնաք", "5": "կրնան" }, "imperfect": { "0": "կրնայի", "1": "կրնայիր", "2": "կրնար", "3": "կրնայինք", "4": "կրնայիք", "5": "կրնային" }, "past": { "0": "կրցայ", "1": "կրցար", "2": "կրցաւ", "3": "կրցանք", "4": "կրցաք", "5": "կրցան" }, "imperative": { "0": "", "1": "կրց՜իր", "2": "", "3": "", "4": "կրցէ՜ք", "5": "" }, "presentPerfect": { "0": "", "1": "", "2": "", "3": "", "4": "", "5": "" }, "pluperfect": { "0": "", "1": "", "2": "", "3": "", "4": "", "5": "" }, "future": { "0": "", "1": "", "2": "", "3": "", "4": "", "5": "" }, "conditional": { "0": "", "1": "", "2": "", "3": "", "4": "", "5": "" } }})
    // }
    //
    //
    //
    var irregularities = irregulars_1.default.find(function (verb) { var _a; return ((_a = verb) === null || _a === void 0 ? void 0 : _a.infinitive) === word; });
    if (word.match(/(.+)ել/)) {
        return new groups_1.Group1(word, irregularities);
    }
    else if (word.match(/(.+)իլ/)) {
        return new groups_1.Group2(word, irregularities);
    }
    else if (word.match(/(.+)ալ/)) {
        return new groups_1.Group3(word, irregularities);
    }
};
exports.default = getWord;

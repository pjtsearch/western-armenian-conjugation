"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var isVowel = function (character) { return ["ա", "ե", "է", "ը", "ի", "ո", "օ"].includes(character); };
exports.default = isVowel;

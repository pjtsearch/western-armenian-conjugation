declare const isVowel: (character: string) => boolean;
export default isVowel;

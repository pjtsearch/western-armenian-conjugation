"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        "infinitive": "ունենալ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "ունեցած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "ունիմ",
                "ունիս",
                "ունի",
                "ունինք",
                "ունիք",
                "ունին"
            ],
            "imperfect": [
                "ունէի",
                "ունէիր",
                "ունէր",
                "ունէինք",
                "ունէիք",
                "ունէին"
            ],
            "past": [
                "ունեցայ",
                "ունեցար",
                "ունեցաւ",
                "ունեցանք",
                "ունեցաք",
                "ունեցան"
            ],
            "imperative": [
                "",
                "ունեցիր",
                "",
                "ունենանք",
                "ունեցէք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "ըսել",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "ըսած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "ըսի",
                "ըսիր",
                "ըսաւ",
                "ըսինք",
                "ըսիք",
                "ըսին"
            ],
            "imperative": [
                "",
                "ըսէ",
                "",
                "",
                "ըսէք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "տալ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "տուած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "տուի",
                "տուիր",
                "տուաւ",
                "տուինք",
                "տուիք",
                "տուին"
            ],
            "imperative": [
                "",
                "տու՜ր",
                "",
                "",
                "տուէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "երթալ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "գացած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "գացի",
                "գացիր",
                "գնաց",
                "գացինք",
                "գացիք",
                "գացին"
            ],
            "imperative": [
                "",
                "գնա՜",
                "",
                "",
                "գացէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "գալ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "եկած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "եկայ",
                "եկար",
                "եկաւ",
                "եկանք",
                "եկաք",
                "եկան"
            ],
            "imperative": [
                "",
                "եկո՜ւր",
                "",
                "",
                "եկէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "ըլլալ",
        "root": "-",
        "group": null,
        "particle": "-",
        "pastParticiple": "եղած",
        "negativeParticiple": "ըլլար",
        "positiveConjugations": {
            "present": [
                "եմ",
                "ես",
                "է",
                "ենք",
                "էք",
                "են"
            ],
            "imperfect": [
                "էի",
                "էիր",
                "էր",
                "էինք",
                "էիք",
                "էին"
            ],
            "past": [
                "եղայ",
                "եղար",
                "եղաւ",
                "եղանք",
                "եղաք",
                "եղան"
            ],
            "imperative": [
                "-",
                "եղի՜ր",
                "-",
                "ըլլա՜նք",
                "եղէ՜ք",
                "-"
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "կարենալ",
        "root": "կրն",
        "group": null,
        "particle": "-",
        "pastParticiple": "կրցած",
        "negativeParticiple": "կրնար",
        "positiveConjugations": {
            "present": [
                "կրնամ",
                "կրնաս",
                "կրնայ",
                "կրնանք",
                "կրնաք",
                "կրնան"
            ],
            "imperfect": [
                "կրնայի",
                "կրնայիր",
                "կրնար",
                "կրնայինք",
                "կրնայիք",
                "կրնային"
            ],
            "past": [
                "կրցայ",
                "կրցար",
                "կրցաւ",
                "կրցանք",
                "կրցաք",
                "կրցան"
            ],
            "imperative": [
                "",
                "կրց՜իր",
                "",
                "",
                "կրցէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "գտնել",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "գտած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "գտայ",
                "գտար",
                "գտաւ",
                "գտանք",
                "գտաք",
                "գտան"
            ],
            "imperative": [
                "",
                "գտի՜ր",
                "",
                "",
                "գտէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "մնալ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "մնացած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "մնացի",
                "մնացիր",
                "մնաց",
                "մնացինք",
                "մնացիք",
                "մնացին"
            ],
            "imperative": [
                "",
                "մնացի՜ր",
                "",
                "",
                "մնացէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "տեսնալ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "տեսած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "տեսայ",
                "տեսար",
                "տեսաւ",
                "տեսանք",
                "տեսաք",
                "տեսան"
            ],
            "imperative": [
                "",
                "տե՜ս",
                "",
                "",
                "տեսէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "առնել",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "առած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "առի",
                "առիր",
                "առաւ",
                "առինք",
                "առիք",
                "առին"
            ],
            "imperative": [
                "",
                "առ՜",
                "",
                "",
                "առէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "գիտնել",
        "root": "",
        "group": null,
        "particle": "-",
        "pastParticiple": "գտած",
        "negativeParticiple": "գիտեր",
        "positiveConjugations": {
            "present": [
                "գիտեմ",
                "գիտես",
                "գիտէ",
                "գիտենք",
                "գիտէք",
                "գիտեն"
            ],
            "imperfect": [
                "գիտէի",
                "գիտէիր",
                "գիտէր",
                "գիտէինք",
                "գիտէիք",
                "գիտէին"
            ],
            "past": [
                "գիտցայ",
                "գիտցար",
                "գիտցաւ",
                "գիտցանք",
                "գիտցաք",
                "գիտցան"
            ],
            "imperative": [
                "",
                "գիտցի՜ր",
                "",
                "",
                "գիտցէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "ուտել",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "կերած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "կերայ",
                "կերար",
                "կերաւ",
                "կերանք",
                "կերաք",
                "կերան"
            ],
            "imperative": [
                "",
                "կե՜ր",
                "",
                "",
                "կերէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperative": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "ընել",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "ըրած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "ըրի",
                "ըրիր",
                "ըրաւ",
                "ըրինք",
                "ըրիք",
                "ըրին"
            ],
            "imperative": [
                "",
                "ըրէ՜",
                "",
                "",
                "ըրէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "անցնիլ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "անցած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "անցայ",
                "անցար",
                "անցաւ",
                "անցանք",
                "անցաք",
                "անցան"
            ],
            "imperative": [
                "",
                "անցի՜ր",
                "",
                "",
                "անցէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "անցնել",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "անցուցած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "անցուցի",
                "անցուցիր",
                "անցուց",
                "անցուցինք",
                "անցուցիք",
                "անցուցին"
            ],
            "imperative": [
                "",
                "անցի՜ր",
                "",
                "",
                "անցէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "բանալ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "բացած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "բացի",
                "բացիր",
                "բացաւ",
                "բացինք",
                "բացիք",
                "բացին"
            ],
            "imperative": [
                "",
                "բա՜ց",
                "",
                "",
                "բացէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "լալ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "լացած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "լացի",
                "լացիր",
                "լացաւ",
                "լացինք",
                "լացիք",
                "լացին"
            ],
            "imperative": [
                "",
                "լա՜ց",
                "",
                "",
                "լացէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "բերել",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "բերած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "բերի",
                "բերիր",
                "բերաւ",
                "բերինք",
                "բերիք",
                "բերին"
            ],
            "imperative": [
                "",
                "բէ՜ր",
                "",
                "",
                "բերէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "դնել",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "դրած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "դրի",
                "դրիր",
                "դրաւ",
                "դրինք",
                "դրիք",
                "դրին"
            ],
            "imperative": [
                "",
                "դի՜ր",
                "",
                "",
                "դրէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "դառնալ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "դարցած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "դարձայ",
                "դարձար",
                "դարձաւ",
                "դարձանք",
                "դարձաք",
                "դարձան"
            ],
            "imperative": [
                "",
                "դարձի՜ր",
                "",
                "",
                "դարձէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "ելլալ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "ելած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "ելայ",
                "ելար",
                "ելաւ",
                "ելանք",
                "ելաք",
                "ելան"
            ],
            "imperative": [
                "",
                "ելի՜ր",
                "",
                "",
                "ելէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "զարնել",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "զարկած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "զարկի",
                "զարկիր",
                "զարկաւ",
                "զարկինք",
                "զարկիք",
                "զարկին"
            ],
            "imperative": [
                "",
                "զա՜րկ",
                "",
                "",
                "զարկէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "թողուլ",
        "root": "թող",
        "group": null,
        "particle": "",
        "pastParticiple": "թողած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperative": [
                "",
                "թո՜ղ",
                "",
                "",
                "թողէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "իյնալ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "ինկած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "ինկայ",
                "ինկար",
                "ինկաւ",
                "ինկանք",
                "ինկաք",
                "ինկան"
            ],
            "imperative": [
                "",
                "ինկի՜ր",
                "",
                "",
                "ինկէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "մեռնիլ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "մեռած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "մեռայ",
                "մեռար",
                "մեռաւ",
                "մեռանք",
                "մեռաք",
                "մեռան"
            ],
            "imperative": [
                "",
                "մեռի՜ր",
                "",
                "",
                "-",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "նստիլ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "նստած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "նստեցայ",
                "նստեցար",
                "նստեցաւ",
                "նստեցանք",
                "նստեցաք",
                "նստեցան"
            ],
            "imperative": [
                "",
                "նստի՜ր",
                "",
                "",
                "նստեցէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "սկսիլ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "սկսած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "սկսայ",
                "սկսար",
                "սկսաւ",
                "սկսանք",
                "սկսաք",
                "սկսան"
            ],
            "imperative": [
                "",
                "սկսէ՜",
                "",
                "",
                "սկսեցէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "տանիլ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "տարած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "տարի",
                "տարիր",
                "տարաւ",
                "տարինք",
                "տարիք",
                "տարին"
            ],
            "imperative": [
                "",
                "տա՜ր",
                "",
                "",
                "տարէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "վախնալ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "վախցած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "վախցայ",
                "վախցար",
                "վախցաւ",
                "վախցանք",
                "վախցաք",
                "վախցան"
            ],
            "imperative": [
                "",
                "վախցի՜ր",
                "",
                "",
                "վախցէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "վկայել",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "",
                "",
                "վկայեցաւ",
                "",
                "",
                ""
            ],
            "imperative": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "խաղալ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "խաղցած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "խաղցայ",
                "խաղցար",
                "խաղցաւ",
                "խաղցանք",
                "խաղցաք",
                "խաղցան"
            ],
            "imperative": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "ստանալ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "ստացայ",
                "ստացար",
                "ստացաւ",
                "ստացանք",
                "ստացաք",
                "ստացան"
            ],
            "imperative": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "կամ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "կամ",
                "կաս",
                "կայ",
                "կանք",
                "կաք",
                "կան"
            ],
            "imperfect": [
                "կայի",
                "կայիր",
                "կար",
                "կայինք",
                "կայիք",
                "կային"
            ],
            "past": [
                "-",
                "-",
                "-",
                "-",
                "-",
                "-"
            ],
            "imperative": [
                "-",
                "-",
                "-",
                "-",
                "-",
                "-"
            ],
            "presentPerfect": [
                "-",
                "-",
                "-",
                "-",
                "-",
                "-"
            ],
            "pluperfect": [
                "-",
                "-",
                "-",
                "-",
                "-",
                "-"
            ],
            "future": [
                "-",
                "-",
                "-",
                "-",
                "-",
                "-"
            ],
            "conditional": [
                "-",
                "-",
                "-",
                "-",
                "-",
                "-"
            ]
        }
    },
    {
        "infinitive": "իջնել",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "իջած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "իջայ",
                "իջար",
                "իջաւ",
                "իջանք",
                "իջաք",
                "իջան"
            ],
            "imperative": [
                "",
                "իջի՜ր",
                "",
                "",
                "իջէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "երեւնալ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "կ'երեւիմ",
                "կ'երեւիս",
                "կ'երեւի",
                "կ'երեւիմ",
                "կ'երեւիմ",
                "կ'երեւիմ"
            ],
            "imperfect": [
                "կ'երեւէի",
                "կ'երեւէիր",
                "կ'երեւէր",
                "կ'երեւէինք",
                "կ'երեւէիք",
                "կ'երեւէին"
            ],
            "past": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperative": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "ըլլալ ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "եղած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "եղայ",
                "եղար",
                "եղաւ",
                "եղանք",
                "եղաք",
                "եղան"
            ],
            "imperative": [
                "-",
                "եղի՜ր",
                "-",
                "ըլլա՜նք",
                "եղէ՜ք",
                "-"
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "զիջիլ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "զիջայ",
                "զիջար",
                "զիջաւ",
                "զիջանք",
                "զիջաք",
                "զիջան"
            ],
            "imperative": [
                "",
                "",
                "",
                "",
                "զիջէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "ընդունիլ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "ընդունած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "ընդունեցի",
                "ընդունեցիր",
                "ընդունեց",
                "ընդունեցինք",
                "ընդունեցիք",
                "ընդունեցին"
            ],
            "imperative": [
                "",
                "ընդունէ՜",
                "",
                "ընդունեցէ՜ք",
                "",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "թքնել",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "թքած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "թքի",
                "թքիր",
                "թքաւ",
                "թքինք",
                "թքիք",
                "թքին"
            ],
            "imperative": [
                "",
                "թու՜ք",
                "",
                "",
                "թքէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "խածնել",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "խածած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "խածի",
                "խածիր",
                "խածաւ",
                "խածինք",
                "խածիք",
                "խածին"
            ],
            "imperative": [
                "",
                "խա՜ծ",
                "",
                "",
                "խածէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "ծնիլ",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "ծնած",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "ծնայ",
                "ծնար",
                "ծնաւ",
                "ծնանք",
                "ծնաք",
                "ծնան"
            ],
            "imperative": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    },
    {
        "infinitive": "մտնել",
        "root": "",
        "group": null,
        "particle": "",
        "pastParticiple": "",
        "negativeParticiple": "",
        "positiveConjugations": {
            "present": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "imperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "past": [
                "մտայ",
                "մտար",
                "մտաւ",
                "մտանք",
                "մտաք",
                "մտան"
            ],
            "imperative": [
                "",
                "մտէ՜",
                "",
                "",
                "մտէ՜ք",
                ""
            ],
            "presentPerfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "pluperfect": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "future": [
                "",
                "",
                "",
                "",
                "",
                ""
            ],
            "conditional": [
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        }
    }
];

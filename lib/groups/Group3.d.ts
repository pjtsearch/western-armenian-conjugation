import Word from "../Word";
export default class Group3 extends Word {
    constructor(word: string, irregularities?: any);
    _getRoot(): string;
    _getPastParticiple(): string;
    _getNegativeParticiple(): string;
    _getImperfectNegativeParticiple(): string;
    _getPositivePresentConjugations(): string[];
    _getPositiveImperfectConjugations(): string[];
    _getPositivePastConjugations(): string[];
    _getPositiveImperativeConjugations(): (string | undefined)[];
    _getPositivePresentPerfectConjugations(): string[];
    _getPositivePluperfectConjugations(): string[];
    _getPositiveFutureConjugations(): string[];
    _getPositiveConditionalConjugations(): string[];
    _getNegativePresentConjugations(): string[];
    _getNegativeImperfectConjugations(): string[];
    _getNegativePastConjugations(): string[];
    _getNegativePresentPerfectConjugations(): string[];
    _getNegativePluperfectConjugations(): string[];
    _getNegativeFutureConjugations(): string[];
    _getNegativeConditionalConjugations(): string[];
}

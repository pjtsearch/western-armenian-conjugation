"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Word_1 = require("../Word");
var isVowel_1 = require("../utilities/isVowel");
var Group1 = /** @class */ (function (_super) {
    __extends(Group1, _super);
    function Group1(word, irregularities) {
        var _this = _super.call(this, word, irregularities) || this;
        _this.group = 1;
        return _this;
    }
    Group1.prototype._getRoot = function () {
        var _a = __read(this.infinitive.match(/(.+)ել/), 2), _ = _a[0], root = _a[1];
        return root;
    };
    Group1.prototype._getPastParticiple = function () {
        if (this.root.slice(-1) === "ն") {
            return this.root + "\u0565\u0581\u0561\u056E";
        }
        else {
            return this.root + "\u0561\u056E";
        }
    };
    Group1.prototype._getNegativeParticiple = function () {
        return this.root + "\u0565\u0580";
    };
    Group1.prototype._getImperfectNegativeParticiple = function () {
        return this.root + "\u0565\u0580";
    };
    Group1.prototype._getPositivePresentConjugations = function () {
        var first, second, third, firstPlural, secondPlural, thirdPlural;
        var base = "" + this.particle + this.root;
        first = base + "\u0565\u0574";
        second = base + "\u0565\u057D";
        third = base + "\u0567";
        firstPlural = base + "\u0565\u0576\u0584";
        secondPlural = base + "\u0567\u0584";
        thirdPlural = base + "\u0565\u0576";
        return [first, second, third, firstPlural, secondPlural, thirdPlural];
    };
    Group1.prototype._getPositiveImperfectConjugations = function () {
        var first, second, third, firstPlural, secondPlural, thirdPlural;
        var base = "" + this.particle + this.root;
        first = base + "\u0567\u056B";
        second = base + "\u0567\u056B\u0580";
        third = base + "\u0567\u0580";
        firstPlural = base + "\u0567\u056B\u0576\u0584";
        secondPlural = base + "\u0567\u056B\u0584";
        thirdPlural = base + "\u0567\u056B\u0576";
        return [first, second, third, firstPlural, secondPlural, thirdPlural];
    };
    Group1.prototype._getPositivePastConjugations = function () {
        var first, second, third, firstPlural, secondPlural, thirdPlural;
        var base = "" + this.root;
        first = base + "\u0565\u0581\u056B";
        second = base + "\u0565\u0581\u056B\u0580";
        third = base + "\u0565\u0581";
        firstPlural = base + "\u0565\u0581\u056B\u0576\u0584";
        secondPlural = base + "\u0565\u0581\u056B\u0584";
        thirdPlural = base + "\u0565\u0581\u056B\u0576";
        return [first, second, third, firstPlural, secondPlural, thirdPlural];
    };
    Group1.prototype._getPositiveImperativeConjugations = function () {
        var first, second, third, firstPlural, secondPlural, thirdPlural;
        var base = "" + this.root;
        second = base + "\u0567";
        firstPlural = base + "\u0565\u0576\u0584";
        secondPlural = base + "\u0565\u0581\u0567\u0584";
        return [first, second, third, firstPlural, secondPlural, thirdPlural];
    };
    Group1.prototype._getPositivePresentPerfectConjugations = function () {
        var first, second, third, firstPlural, secondPlural, thirdPlural;
        var base = this.pastParticiple;
        first = base + " \u0565\u0574";
        second = base + " \u0565\u057D";
        third = base + " \u0567";
        firstPlural = base + " \u0565\u0576\u0584";
        secondPlural = base + " \u0565\u0584";
        thirdPlural = base + " \u0565\u0576";
        return [first, second, third, firstPlural, secondPlural, thirdPlural];
    };
    Group1.prototype._getPositivePluperfectConjugations = function () {
        var first, second, third, firstPlural, secondPlural, thirdPlural;
        var base = this.pastParticiple;
        first = base + " \u0567\u056B";
        second = base + " \u0567\u056B\u0580";
        third = base + " \u0567\u0580";
        firstPlural = base + " \u0567\u056B\u0576\u0584";
        secondPlural = base + " \u0567\u056B\u0584";
        thirdPlural = base + " \u0567\u056B\u0576";
        return [first, second, third, firstPlural, secondPlural, thirdPlural];
    };
    Group1.prototype._getPositiveFutureConjugations = function () {
        var first, second, third, firstPlural, secondPlural, thirdPlural;
        var base = "\u057A\u056B\u057F\u056B " + this.futureParticiple;
        first = base + "\u0565\u0574";
        second = base + "\u0565\u057D";
        third = base + "\u0567";
        firstPlural = base + "\u0565\u0576\u0584";
        secondPlural = base + "\u0567\u0584";
        thirdPlural = base + "\u0565\u0576";
        return [first, second, third, firstPlural, secondPlural, thirdPlural];
    };
    Group1.prototype._getPositiveConditionalConjugations = function () {
        var first, second, third, firstPlural, secondPlural, thirdPlural;
        var base = "\u057A\u056B\u057F\u056B " + this.futureParticiple;
        first = base + "\u0567\u056B";
        second = base + "\u0567\u056B\u0580";
        third = base + "\u0567\u0580";
        firstPlural = base + "\u0567\u056B\u0576\u0584";
        secondPlural = base + "\u0567\u056B\u0584";
        thirdPlural = base + "\u0567\u056B\u0576";
        return [first, second, third, firstPlural, secondPlural, thirdPlural];
    };
    Group1.prototype._getNegativePresentConjugations = function () {
        var first, second, third, firstPlural, secondPlural, thirdPlural;
        var base = "" + this.negativeParticiple;
        first = "\u0579\u0565\u0574 " + base;
        second = "\u0579\u0565\u057D " + base;
        third = isVowel_1.default(base.charAt(0)) ? "\u0579'" + base : "\u0579\u056B " + base;
        firstPlural = "\u0579\u0565\u0576\u0584 " + base;
        secondPlural = "\u0579\u0565\u0584 " + base;
        thirdPlural = "\u0579\u0565\u0576 " + base;
        return [first, second, third, firstPlural, secondPlural, thirdPlural];
    };
    Group1.prototype._getNegativeImperfectConjugations = function () {
        var first, second, third, firstPlural, secondPlural, thirdPlural;
        var base = "" + this.imperfectNegativeParticiple;
        first = "\u0579\u0567\u056B " + base;
        second = "\u0579\u0567\u056B\u0580 " + base;
        third = "\u0579\u0567\u0580 " + base;
        firstPlural = "\u0579\u0567\u056B\u0576\u0584 " + base;
        secondPlural = "\u0579\u0567\u056B\u0584 " + base;
        thirdPlural = "\u0579\u0567\u056B\u0576 " + base;
        return [first, second, third, firstPlural, secondPlural, thirdPlural];
    };
    Group1.prototype._getNegativePastConjugations = function () {
        var first, second, third, firstPlural, secondPlural, thirdPlural;
        var base = this.positiveConjugations.past;
        first = "\u0579" + base[0];
        second = "\u0579" + base[1];
        third = "\u0579" + base[2];
        firstPlural = "\u0579" + base[3];
        secondPlural = "\u0579" + base[4];
        thirdPlural = "\u0579" + base[5];
        return [first, second, third, firstPlural, secondPlural, thirdPlural];
    };
    // _getNegativeImperativeConjugations():Array<string|undefined>{return []}
    Group1.prototype._getNegativePresentPerfectConjugations = function () {
        var first, second, third, firstPlural, secondPlural, thirdPlural;
        var base = "" + this.pastParticiple;
        first = base + " \u0579\u0565\u0574";
        second = base + " \u0579\u0565\u057D";
        third = base + " \u0579\u0567";
        firstPlural = base + " \u0579\u0565\u0576\u0584";
        secondPlural = base + " \u0579\u0565\u0584";
        thirdPlural = base + " \u0579\u0565\u0576";
        return [first, second, third, firstPlural, secondPlural, thirdPlural];
    };
    Group1.prototype._getNegativePluperfectConjugations = function () {
        var first, second, third, firstPlural, secondPlural, thirdPlural;
        var base = "" + this.pastParticiple;
        first = base + " \u0579\u0567\u056B";
        second = base + " \u0579\u0567\u056B\u0580";
        third = base + " \u0579\u0567\u0580";
        firstPlural = base + " \u0579\u0567\u056B\u0576\u0584";
        secondPlural = base + " \u0579\u0567\u056B\u0584";
        thirdPlural = base + " \u0579\u0567\u056B\u0576";
        return [first, second, third, firstPlural, secondPlural, thirdPlural];
    };
    Group1.prototype._getNegativeFutureConjugations = function () {
        var first, second, third, firstPlural, secondPlural, thirdPlural;
        var base = "\u057A\u056B\u057F\u056B \u0579" + this.futureParticiple;
        first = base + "\u0565\u0574";
        second = base + "\u0565\u057D";
        third = base + "\u0567";
        firstPlural = base + "\u0565\u0576\u0584";
        secondPlural = base + "\u0567\u0584";
        thirdPlural = base + "\u0565\u0576";
        return [first, second, third, firstPlural, secondPlural, thirdPlural];
    };
    Group1.prototype._getNegativeConditionalConjugations = function () {
        var first, second, third, firstPlural, secondPlural, thirdPlural;
        var base = "\u057A\u056B\u057F\u056B \u0579" + this.futureParticiple;
        first = base + "\u0567\u056B";
        second = base + "\u0567\u056B\u0580";
        third = base + "\u0567\u0580";
        firstPlural = base + "\u0567\u056B\u0576\u0584";
        secondPlural = base + "\u0567\u056B\u0584";
        thirdPlural = base + "\u0567\u056B\u0576";
        return [first, second, third, firstPlural, secondPlural, thirdPlural];
    };
    return Group1;
}(Word_1.default));
exports.default = Group1;

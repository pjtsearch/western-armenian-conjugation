# Western Armenian Verb Conjugation Library (WIP)

This library conjugates Western Armenian verbs.  **It is currently not reliable for correct information.**

## What doesn't work right yet:
- Negative imperative conjugations
- ըլլալ

## Installation
     yarn add "https://gitlab.com/pjtsearch/western-armenian-conjugation.git"
     #or
	 npm install "https://gitlab.com/pjtsearch/western-armenian-conjugation.git"

## Documentation
[TypeDoc](https://western-armenian-conjugation.netlify.com/ "tsdoc")

## Example
	const {getWord} = require("western-armenian-conjugation")
	let word = getWord("դնել")
	console.log(word)
	//Logs:
	//  {
	// 	  group: 1,
	// 	  negativeConjugations: {
	// 		present: [
	// 		  'չեմ դներ',
	// 		  'չես դներ',
	// 		  'չի դներ',
	// 		  'չենք դներ',
	// 		  'չեք դներ',
	// 		  'չեն դներ'
	// 		],
	// 		imperfect: [
	// 		  'չէի դներ',
	// 		  'չէիր դներ',
	// 		  'չէր դներ',
	// 		  'չէինք դներ',
	// 		  'չէիք դներ',
	// 		  'չէին դներ'
	// 		],
	// 		past: [ 'չդրի', 'չդրիր', 'չդրաւ', 'չդրինք', 'չդրիք', 'չդրին' ],
	// 		imperative: [],
	// 		presentPerfect: [
	// 		  'դրած չեմ',
	// 		  'դրած չես',
	// 		  'դրած չէ',
	// 		  'դրած չենք',
	// 		  'դրած չեք',
	// 		  'դրած չեն'
	// 		],
	// 		pluperfect: [
	// 		  'դրած չէի',
	// 		  'դրած չէիր',
	// 		  'դրած չէր',
	// 		  'դրած չէինք',
	// 		  'դրած չէիք',
	// 		  'դրած չէին'
	// 		],
	// 		future: [
	// 		  'պիտի չդնեմ',
	// 		  'պիտի չդնես',
	// 		  'պիտի չդնէ',
	// 		  'պիտի չդնենք',
	// 		  'պիտի չդնէք',
	// 		  'պիտի չդնեն'
	// 		],
	// 		conditional: [
	// 		  'պիտի չդնէի',
	// 		  'պիտի չդնէիր',
	// 		  'պիտի չդնէր',
	// 		  'պիտի չդնէինք',
	// 		  'պիտի չդնէիք',
	// 		  'պիտի չդնէին'
	// 		]
	// 	  },
	// 	  irregularities: {
	// 		present: [ '', '', '', '', '', '' ],
	// 		imperfect: [ '', '', '', '', '', '' ],
	// 		past: [ 'դրի', 'դրիր', 'դրաւ', 'դրինք', 'դրիք', 'դրին' ],
	// 		imperative: [ '', 'դի՜ր', '', '', 'դրէ՜ք', '' ],
	// 		presentPerfect: [ '', '', '', '', '', '' ],
	// 		pluperfect: [ '', '', '', '', '', '' ],
	// 		future: [ '', '', '', '', '', '' ],
	// 		conditional: [ '', '', '', '', '', '' ]
	// 	  },
	// 	  infinitive: 'դնել',
	// 	  root: 'դն',
	// 	  futureParticiple: 'դն',
	// 	  particle: 'կը ',
	// 	  pastParticiple: 'դրած',
	// 	  negativeParticiple: 'դներ',
	// 	  imperfectNegativeParticiple: 'դներ',
	// 	  positiveConjugations: {
	// 		present: [
	// 		  'կը դնեմ',
	// 		  'կը դնես',
	// 		  'կը դնէ',
	// 		  'կը դնենք',
	// 		  'կը դնէք',
	// 		  'կը դնեն'
	// 		],
	// 		imperfect: [
	// 		  'կը դնէի',
	// 		  'կը դնէիր',
	// 		  'կը դնէր',
	// 		  'կը դնէինք',
	// 		  'կը դնէիք',
	// 		  'կը դնէին'
	// 		],
	// 		past: [ 'դրի', 'դրիր', 'դրաւ', 'դրինք', 'դրիք', 'դրին' ],
	// 		imperative: [ undefined, 'դի՜ր', undefined, 'դնենք', 'դրէ՜ք', undefined ],
	// 		presentPerfect: [
	// 		  'դրած եմ',
	// 		  'դրած ես',
	// 		  'դրած է',
	// 		  'դրած ենք',
	// 		  'դրած եք',
	// 		  'դրած են'
	// 		],
	// 		pluperfect: [
	// 		  'դրած էի',
	// 		  'դրած էիր',
	// 		  'դրած էր',
	// 		  'դրած էինք',
	// 		  'դրած էիք',
	// 		  'դրած էին'
	// 		],
	// 		future: [
	// 		  'պիտի դնեմ',
	// 		  'պիտի դնես',
	// 		  'պիտի դնէ',
	// 		  'պիտի դնենք',
	// 		  'պիտի դնէք',
	// 		  'պիտի դնեն'
	// 		],
	// 		conditional: [
	// 		  'պիտի դնէի',
	// 		  'պիտի դնէիր',
	// 		  'պիտի դնէր',
	// 		  'պիտի դնէինք',
	// 		  'պիտի դնէիք',
	// 		  'պիտի դնէին'
	// 		]
	// 	  }
	// 	}

## Credits
- Irregular conjugations and inspiration from [ma6.free.fr](http://ma6.free.fr "ma6.free.fr")
